var config = require('./config.json');
var mysql = require('mysql2');
var bodyParser = require('body-parser');
var request = require("request");
var extend = require("extend");
var sendpulse = require("./api/sendpulse.js");

var db = mysql.createConnection(config.mysql);

module.exports = function (app) {

    app.get('/...', function (req, res) {
        RenderForm(res);
    });

    app.get('/...', function (req, res) {
        res.status(200).render("...");
    });

    app.get('/...', function (req, res) {
        res.status(200).render("...");
    });

    app.post('/...', bodyParser.urlencoded({extended: true}), function (req, res) {
        var form = req.body;
        var surname = (form.surname || '').trim();
        var firstname = (form.firstname || '').trim();
        var studentID = (form.studentID || '').trim();
        var phone = (form.phone || '').trim().replace(/[^\d]/g, '');
        var email = (form.email || '').trim();
        var pin = (form.pin || '').trim();
        var segment = (form.segmentID || '').trim();
        var validation = surname && firstname && studentID && phone.length === 10;
        var elem = surname + firstname + studentID;
        var PINsend = "";
        if (validation) {
            db.query(
                'SELECT `pin` FROM `students` WHERE `element` = MD5(LOWER(?))',
                [
                    elem
                ],
                function (err, data) {
                    if (err || data.length < 1) {
                        res.status(500);
                        RenderForm(res, {
                            surname: surname,
                            firstname: firstname,
                            studentID: studentID,
                            email: email,
                            phone: phone,
                            segmentID: segment,
                            message: 'Проверьте корректность введенных данных!'
                        });
                    } else {
                        if (data.length > 0) {
                            PINsend = data[0].pin;
                        }
                        if (data.length > 0 && data[0].pin == pin) {
                            db.query(
                                'UPDATE `students` SET `status` = ?, `email` = ?, `phone` = ?, `dateupdate` = now() WHERE `element` = MD5(LOWER(?))',
                                [
                                    segment,
                                    email,
                                    phone,
                                    elem
                                ],
                                function (err, data) {
                                    if (err) {
                                        res.status(500);
                                        RenderForm(res, {
                                            surname: surname,
                                            firstname: firstname,
                                            studentID: studentID,
                                            email: email,
                                            phone: phone,
                                            segmentID: segment,
                                            message: 'Произошла ошибка, попробуйте позже1'
                                        });
                                    } else {
                                        WriteDB(res, {
                                            surname: surname,
                                            firstname: firstname,
                                            studentID: studentID,
                                            email: email,
                                            phone: phone,
                                            segment: segment,
                                            elem: elem,
                                            pin: PINsend
                                        });
                                    }
                                }
                            )
                        } else {
                            db.query(
                                'SELECT `status` FROM `students` WHERE `element` = MD5(LOWER(?))',
                                [
                                    elem
                                ],
                                function (err, data) {
                                    if (err) {
                                        res.status(500);
                                        RenderForm(res, {
                                            surname: surname,
                                            firstname: firstname,
                                            studentID: studentID,
                                            email: email,
                                            phone: phone,
                                            segmentID: segment,
                                            message: 'Произошла ошибка, попробуйте позже2',
                                            ButName: "Записаться"
                                        });
                                    } else {
                                        if (data.length > 0 && data[0].status == null) {
                                            WriteDB(res, {
                                                surname: surname,
                                                firstname: firstname,
                                                studentID: studentID,
                                                email: email,
                                                phone: phone,
                                                segment: segment,
                                                elem: elem,
                                                pin: PINsend
                                            });
                                        } else {
                                            RenderForm(res, {
                                                surname: surname,
                                                firstname: firstname,
                                                studentID: studentID,
                                                segmentID: segment,
                                                email: email,
                                                phone: phone,
                                                pinRow: true,
                                                disabled: "readonly",
                                                message: 'Вы уже записаны! Для потверждения изменения даты и времени на выбранные ниже укажите Ваш PIN',
                                                ButName: "Перезаписаться"
                                            });
                                        }
                                    }
                                }
                            )
                        }
                    }
                }
            )
        } else {
            res.status(400);
            RenderForm(res, {
                surname: surname,
                firstname: firstname,
                studentID: studentID,
                segmentID: segment,
                email: email,
                phone: phone,
                message: 'Заполните форму корректно!'
            });
        }
    });

    app.post('/...', bodyParser.urlencoded({extended: true}), function (req, res) {
        var form = req.body;
        var surname = (form.surname || '').trim();
        var firstname = (form.firstname || '').trim();
        var studentID = (form.studentID || '').trim();
        var pin = (form.pin || '').trim();
        var validation = surname && firstname && studentID;
        var elem = surname + firstname + studentID;
        if (validation) {
            db.query(
                'SELECT `pin` FROM `students` WHERE `element` = MD5(LOWER(?))',
                [
                    elem
                ],
                function (err, data) {
                    if (err || data.length < 1) {
                        res.status(500).render("donorday-form-cancel", {
                            surname: surname,
                            firstname: firstname,
                            studentID: studentID,
                            message: 'Проверьте корректность введенных данных!'
                        });
                    } else {
                        if (data.length > 0 && data[0].pin == pin) {
                            db.query(
                                'UPDATE `students` SET `status` = NULL WHERE `element` = MD5(LOWER(?))',
                                [
                                    elem
                                ],
                                function (err, data) {
                                    if (err) {
                                        res.status(500).render("donorday-form-cancel", {
                                            surname: surname,
                                            firstname: firstname,
                                            studentID: studentID,
                                            message: 'Произошла ошибка, попробуйте позже'
                                        });
                                    } else {
                                        res.status(200).render("donorday-success-cancel");
                                    }
                                }
                            )
                        } else {
                            res.status(500).render("donorday-form-cancel", {
                                surname: surname,
                                firstname: firstname,
                                studentID: studentID,
                                message: 'Проверьте корретность введенного PIN'
                            });
                        }
                    }
                }
            )
        } else {
            res.status(400).render("donorday-form-cancel", {
                surname: surname,
                firstname: firstname,
                studentID: studentID,
                message: 'Заполните форму корректно!'
            });
        }
    });
};

function WriteDB(res, formData) {
    formData = formData || {};
    db.query(
        'SELECT `segments`.`count` - COUNT(`students`.`status`) AS `lostcount` FROM `students`, `segments` WHERE `students`.`status` = ? AND `segments`.`segmentID` = ?',
        [
            formData.segment,
            formData.segment
        ],
        function (err, data) {
            if (err) {
                res.status(500);
                RenderForm(res, {
                    surname: formData.surname,
                    firstname: formData.firstname,
                    studentID: formData.studentID,
                    email: formData.email,
                    phone: formData.phone,
                    segmentID: formData.segment,
                    message: 'Произошла ошибка, попробуйте позже!',
                    ButName: "Записаться"
                });
            } else {
                if (data[0].lostcount > 0){
                    db.query(
                        'UPDATE `students` SET `status` = ?, `email` = ?, `phone` = ?, `dateupdate` = now() WHERE `element` = MD5(LOWER(?))',//REPLACE(LOWER(?), " ", ""))',
                        [
                            formData.segment,
                            formData.email,
                            formData.phone,
                            formData.elem
                        ],
                        function (err, data) {
                            if (err) {
                                res.status(500);
                                RenderForm(res, {
                                    surname: formData.surname,
                                    firstname: formData.firstname,
                                    studentID: formData.studentID,
                                    email: formData.email,
                                    phone: formData.phone,
                                    segmentID: formData.segment,
                                    message: 'Произошла ошибка, попробуйте позже',
                                    ButName: "Записаться"
                                });
                            } else {
                                db.query(
                                    'SELECT `segmentDate`,`segmentTime` FROM `segments` WHERE `segmentID` = ?',
                                    [
                                        formData.segment
                                    ],
                                    function (err, data) {
                                        if (err) {
                                            res.status(500);
                                            RenderForm(res, {
                                                surname: surname,
                                                firstname: firstname,
                                                studentID: studentID,
                                                email: email,
                                                phone: phone,
                                                segmentID: segment,
                                                message: 'Произошла ошибка, попробуйте позже',
                                                ButName: "Записаться"
                                            });
                                        } else {
                                            var name = formData.surname + " " + formData.firstname;
                                            var date = data[0].segmentDate;
                                            var time = data[0].segmentTime;
                                            var txt = "...";
                                            var message = '...';
                                            request(message, function (error, response, body) {
                                            });

                                            sendpulse.init();
                                            var text = "..."
                                            var emails = {
                                                ...
                                                }]
                                            };

                                            var answerGetter = function answerGetter(data) {
                                            };
                                            sendpulse.smtpSendMail(answerGetter, emails);

                                            res.status(200).render('donorday-success', {
                                                segmentTime: time,
                                                segmentDate: date
                                            });

                                        }
                                    }
                                )
                            }
                        }
                    )
                } else {
                    RenderForm(res, {
                        surname: formData.surname,
                        firstname: formData.firstname,
                        studentID: formData.studentID,
                        email: formData.email,
                        phone: formData.phone,
                        message: 'Извините, но мест на выбранное Вами время больше не осталось!',
                        ButName: "Записаться"
                    });
                }
            }
        }
    );

}

function RenderForm(res, formData) {
    formData = formData || {};

    db.query(
        'SELECT * FROM `segments` `seg`' +
        '    LEFT JOIN (' +
        '        SELECT `status` as `id`, COUNT(`recordID`) as `count` FROM `students` GROUP BY `id`' +
        '    ) `stu` ON `seg`.`segmentID` = `stu`.`id`' +
        'WHERE `seg`.`count` > `stu`.`count` OR `stu`.`count` IS NULL',
        function (err, rows) {
            if (err) {
                res.status(500).send('error');
                return;
            }

            var days = rows.reduce(function (res, row) {
                res[row.segmentDate] = res[row.segmentDate] || [];
                res[row.segmentDate].push({
                    id: row.segmentID,
                    time: row.segmentTime,
                    checked: formData.segmentID == row.segmentID ? ' checked' : ''
                });

                return res;
            }, {});

            days = Object.keys(days).map(function (day) {
                return {
                    date: day,
                    time: days[day]
                };
            });
            if (rows.length != 0) {
                res.render('donorday-form', extend({
                    showTables: true,
                    showWarning: false,
                    segments: days,
                    ButName: "Записаться"
                }, formData));
            } else {
                res.render('donorday-form', {
                    showTables: false,
                    showWarning: true
                });
            }
        }
    );
}